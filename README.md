# Quick Links

## Development
- Test Plasma Applet/Plasmoid/Widget: https://develop.kde.org/docs/plasma/widget/testing/#plasmoidviewer
- Plasma Mobile Windowed: https://community.kde.org/Get_Involved/development/Build_software_with_kdesrc-build#Plasma_Mobile
- Plasma Windowed: `QT_QPA_PLATFORM=wayland dbus-run-session kwin_wayland --xwayland "plasmashell" --width 1366 --height 768`
- Getting Started/Involved: https://community.kde.org/Get_Involved
- Kdesrc-build Plasma 6: https://community.kde.org/Get_Involved/development/More#kdesrc-build,_Qt6_and_KDE_Frameworks_6
- How to use GitLab: https://community.kde.org/Infrastructure/GitLab
- Merge Request Keywords: https://community.kde.org/Policies/Commit_Policy#Special_keywords_in_GIT_and_SVN_log_messages
- How to get a Developer Account: https://community.kde.org/Infrastructure/Get_a_Developer_Account

## Bug Fixing
- Juniour Jobs: https://go.kde.org/jj
- Better Crash Reports: https://community.kde.org/Guidelines_and_HOWTOs/Debugging/How_to_create_useful_crash_reports

## Communication
- KDE Matrix Rooms/Channels: https://community.kde.org/Matrix#Rooms
- Krita is #krita on Libera Chat

## General
- KDE Schedules: https://community.kde.org/Schedules

## Policy
- Code Licensing: https://community.kde.org/Policies/Licensing_Policy#Policy

## SysAdmin
- Open a SysAdmin Ticket: https://go.kde.org/systicket

## PIM
- Start Akonadi with `kdesrc-build` https://community.kde.org/KDE_PIM/Development#Running_KDE_PIM
